﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace API_AT.API.Models
{
    public class AuthModel
    {
        public string accessToken { get; set; }
        public string tokenType { get; set; }
    }
}
