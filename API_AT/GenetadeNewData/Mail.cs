﻿using System;

namespace NewMail
{
    public static class TempEmail
    {
        const string name0 = "test";
        const string name1 = "nAstya";
        const string name2 = "lifE";

        public static string GenerateMail()
        {
            string randomEmail = "";
            Random random = new Random();
            var name = random.Next(0, 3);
            switch (name)
            {
                case 0:
                    randomEmail += name0;
                    break;
                case 1:
                    randomEmail += name1;
                    break;
                case 2:
                    randomEmail += name2;
                    break;
            }
            randomEmail += (DateTime.Now.ToBinary() * -1).ToString() + "@gmail.com";
            return randomEmail;
        }
    }
}
